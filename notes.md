# Notes

This is just a collection of questions and shortcomings that I find in the Arti docs as I code using its APIs

1.- How to use bridges and pluggable transports in Arti powered programs

Hint: look at doc/bridges.md

nickm:

> ok; so the main configuration file for arti is arti.toml

> it's a toml-formatted text file that turns things on and off, and it's used to set the configuration for a running arti process

> if you're writing code, you can also build configuration objects by hand and use them to configure (or reconfigure) a TorClient object

> the file crates/arti/arti-example-config.toml documents the file and its options

Updates: 

- `config::BridgesConfig` in `arti_client` is the preferred way to create a bridge connection in Tor.

Feedback:

- No example is there on how to use Snowflake using `BridgesConfig`. The only example given is that of `obfs4`, which, although is great, is a different protocol and concept than Snowflake, with its own potential set of options.

This should be rectified for ease of use, or if these options are already present somewhere, we can add some links to those docs perhaps.

-  So far, on a preliminary scrolling of `arti-client`'s docs, I have seen that even if there is information, it is sometimes not linked to each other in an easy-to-find manner.

The `BridgesConfig` is a good example, it was after some discussion with the Arti devs and some separate searching of all the items in the crate that I was able to find it, even though connecting using bridges is a critical feature of Tor and Arti.

2. The docs to use obfs4 weren't very clear on what the "path" was. Some examples hardcoded `/usr/bin/obfs4proxy`, which was unnecessarily confusing

3. Understanding `arti.toml` was not as easy as it seemed, eg to activate bridge properly I had to set a couple more parameters than initially seemed (this could just be user error though)

4. The code didn't really give any good indication of the error that may result from #2 (path to proxy not being defined right), something as simple as "hey, I couldn't find `obfs4proxy`, can you help me find it/is it installed?" would be helpful

